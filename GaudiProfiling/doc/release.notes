!-----------------------------------------------------------------------------
! Package     : GaudiProfiling
! Responsible : Chris Jones, Karol Kruzelecki
! Purpose     : Profiling and performance monitoring of Gaudi/LHCb software
! Commit Id   : $Format:%H$
!-----------------------------------------------------------------------------

============================ GaudiProfiling v2r9 =============================

! 2016-03-10 - commit 4a18175

 - removed uses of templates implements[1-4], extends[1-4] and
   extend_interfaces[1-4]

   Since merge request !22 they are unnecessary.
    See merge request !133

! 2016-02-11 - commit b0618f7

 - Improve CommonMessaging

   Implementation of GAUDI-1146: Improve CommonMessaging and use it in more
   places.

   See merge request !76

   Conflicts:
   GaudiExamples/tests/qmtest/refs/AlgTools2.ref
   GaudiExamples/tests/qmtest/refs/MultiInput/Read.ref
   GaudiExamples/tests/qmtest/refs/conditional_output/write.ref
   GaudiKernel/GaudiKernel/AlgTool.h
   GaudiKernel/GaudiKernel/Algorithm.h
   GaudiKernel/GaudiKernel/Service.h
   GaudiKernel/src/Lib/AlgTool.cpp
   GaudiKernel/src/Lib/Algorithm.cpp


! 2016-02-08 - commit 9454bdf

 - Gaudi service for Jemalloc profiling (JemallocProfileSvc)

   Jemalloc profiler implemented as a GaudiService instead of an algorithm. This
   allows profiling without modifying the algorithm sequence.
    See merge request !93
    See LBCORE-1012.
============================ GaudiProfiling v2r8 =============================

! 2016-01-07 - commit fabc739

 - fixed compilation with clang 3.7 (after hive merge)

   * fixed compilation problems related to `operator<<` (gcc is more
    permissive than clang).
   * fixed also some clang warnings.
    Fixes GAUDI-1157.
    See merge request !84

! 2015-11-02 - commit 57f356c

 - Merge branch 'hive' into 'master'

   Fixes GAUDI-978.

   See merge request !65

============================ GaudiProfiling v2r7 =============================

! 2015-10-08 - commit c50c176

 - improvements to documentation and release tools

   See merge request !43


! 2015-09-25 - commit 35dd00c

 - Merge branch 'dev-smartif-use' into 'master'

   Provide (and use) C++11 smart pointer 'look and feel' for SmartIF

   The aim of this branch is to confine, for everything that inherits from
   IInterface, the calls to addRef(), release() and queryInterface() to the
   SmartIF implementation. Exceptions are a few places where interfaces
   (currently) return bare pointers (instead of SmartIF...) and where one thus
   has to addRef() explicitly to avoid returning a dangling pointer. This can be
   avoided by changing the relevant interface to return a SmartIF instead of a
   bare pointer.

   In addition, make SmartIF 'look and feel' like a smart pointer.

   - use explict bool conversion instead of .isValid()
   - add SmartIF::as<IFace>(), to return a SmartIF<IFace> to an alternate
      interface -- which (together with move) encourages the use of auto
   - add ISvcLocator::as<IFace>(), to return a SmartIF<IFace> to the current
   ISvcLocator.
   - add ServiceManager::service<IFace>() which return SmartIF<IFace> which
   encourages
      the use of auto

   And add a few other C++11 modernizations (eg. prefer STL over raw loop)

   Fixes GAUDI-1094

   See merge request !24


! 2015-09-11 - commit c062cbe

 - C++11 modernization changes

   Some trivial - and some not so trivial! - changes which take advantage of
   C++11...

   See merge request !7

============================ GaudiProfiling v2r6 =============================
! 2015-06-19 - Ben Couturier
 - Added Jemalloc profiling feature to gaudirun.py

! 2015-06-18 - Marco Clemencic
 - Added test for GAUDI-1045 (segfault with LD_PRELOAD=libjemalloc.so).

! 2015-06-16 - Ben Couturier
 - Added JemallocProfile algorithm to dump heap with Jemalloc at regular
 intervals.

============================ GaudiProfiling v2r5 =============================
! 2015-05-12 - Marco Clemencic
 - Modified GoogleAuditor.cpp to support new gperftools headers path.

============================ GaudiProfiling v2r4 =============================
! 2014-07-06 - Marco Clemencic
 - Modified interlprofiler.md to use Doxygen flavour of markdown.

============================ GaudiProfiling v2r3 =============================
! 2014-11-11 - Chris Jones
 - Fix implementation of Google profilers to properly support auditing during
   different processing phases.

============================ GaudiProfiling v2r2 =============================
! 2014-08-27 - Ben Couturier
 - LBCORE-587: CallgrindProfile algorithm to enable/disable Callgrind profile

============================ GaudiProfiling v2r1 =============================
! 2014-03-18 - Ben Couturier
 - Fixed bug #104127: remove hwaf configuration.

============================ GaudiProfiling v2r0 =============================
! 2013-12-11 - Sebastien Binet
 - Added hwaf configuration files.

! 2013-07-26 - Marco Clemencic
 - Ported new GaudiPluginService to CMT.

! 2013-07-19 - Marco Clemencic
 - Clean up in the use of the new PluginService.

! 2013-07-18 - Marco Clemencic
 - Removed use of obsolete AlgFactory.h, SvcFactory.h, ToolFactory.h,
   AudFactory.h and CnvFactory.h.

============================ GaudiProfiling v1r8 =============================
! 2013-03-05 - Hubert Degaudenzi
 - Fixed compilation on Fedora Core 17 when VTune is missing.

============================ GaudiProfiling v1r7 =============================
! 2012-10-21 - Marco Clemencic
 - Simplified the work-around for compilation with -std=c++0x.

============================ GaudiProfiling v1r6 =============================
! 2012-07-27 - Marco Clemencic
 - Rationalized the permissions of files in the repository.

! 2012-07-26 - Benedikt Hegner
 - Fixed compilation with -std=c++0x.

============================ GaudiProfiling v1r5 =============================
! 2012-06-08 - Marco Clemencic
 - Added CMake configuration files.

! 2012-06-06 - Marco Clemencic
 - Disable on MacOSX.

! 2012-05-03 - Marco Clemencic
 - Fixed some "pedantic" warnings.

============================ GaudiProfiling v1r4 =============================
! 2012-03-13 - Marco Clemencic
 - Modified to compile IntelProfiler only on x86_64 and if the VTune library is
   available (from AFS).
 - Minor clean up and hidden a warning in IntelProfiler.

! 2012-02-24 - Sasha Mazurov
 - Patch #5209: add IntelProfiler component
   (see doc/intelprofiler.md)

============================ GaudiProfiling v1r3 =============================
! 2012-02-25 - Chris Jones
 - Fix a bug in the Google auditors that prevented consecutive events from
   being audited.

! 2012-02-22 - Chris Jones
 - Patch #4852: Auditors based on the Google Perf Tools
   Note: Google auditors can be instantiated only if the libraries tcmalloc or
         profiler are preloaded (e.g. with gaudirun.py --preload).

============================ GaudiProfiling v1r2 =============================
! 2011-10-12 - Hubert Degaudenzi
 - added cstddef include for the size_t type definition which apparently has been
   moved in Gcc 4.6.

============================ GaudiProfiling v1r1 =============================
! 2011-06-23 - Hubert Degaudenzi
 - fixed warnings from the eclipse code analysis engine.

! 2011-05-05 - Marco Clemencic
 - Disabled on i386, for 32 bits MacOSX.

============================== GaudiProfiling v1r0 ===========================
! 2011-01-19 - Marco Clemencic
 - Fixed ICC warnings.

! 2011-01-16 - Karol Kruzelecki
 - new package
