Package : ParticleProp
Package manager : Charles Leggett
Commit Id: $Format:%H$

============================== PartPropSvc v7r1 ==============================

! 2016-03-10 - commit 4a18175

 - removed uses of templates implements[1-4], extends[1-4] and
   extend_interfaces[1-4]

   Since merge request !22 they are unnecessary.
    See merge request !133

! 2016-02-11 - commit b0618f7

 - Improve CommonMessaging

   Implementation of GAUDI-1146: Improve CommonMessaging and use it in more
   places.

   See merge request !76

   Conflicts:
   GaudiExamples/tests/qmtest/refs/AlgTools2.ref
   GaudiExamples/tests/qmtest/refs/MultiInput/Read.ref
   GaudiExamples/tests/qmtest/refs/conditional_output/write.ref
   GaudiKernel/GaudiKernel/AlgTool.h
   GaudiKernel/GaudiKernel/Algorithm.h
   GaudiKernel/GaudiKernel/Service.h
   GaudiKernel/src/Lib/AlgTool.cpp
   GaudiKernel/src/Lib/Algorithm.cpp

============================== PartPropSvc v7r0 ==============================

! 2015-10-27 - commit e585b09

 - PartPropSvc: patch for bad parsing of input files: GAUDI-1119

   In the PartPropSvc, after upgrade to boost::regex, parsing of input files
   fails silently if no "=" is present in the token (ie file type is not
   specified), and the file loaded will be the default table instead of the user
   supplied one.

   eg: InputFile = "myPDGTABLE.MeV" will result in "PDGTABLE.MeV" being loaded
   instead, with no message that this has taken place.

   Fixes GAUDI-1119.

   See merge request !59


! 2015-10-26 - commit de80db5

 - More modernization changes

   Fix (almost) all warnings from clang 3.7, and use clang-modernize to further
   modernize the code.

   Fixes GAUDI-1118.

   See merge request !49

============================== PartPropSvc v6r4 ==============================

! 2015-09-11 - commit c062cbe

 - C++11 modernization changes

   Some trivial - and some not so trivial! - changes which take advantage of
   C++11...

   See merge request !7

============================== PartPropSvc v6r3 ==============================
! 2015-05-21 - Marco Clemencic
 - Fixed compilation with CMT.

! 2015-04-15 - Marco Clemencic
 - GAUDI-1024: Replaced Tokenizer with regular expression.

============================== PartPropSvc v6r2 ==============================
! 2014-10-07 - Charles Leggett
 - added missing neutrinos and gluon to PDGTABLE.MeV and PDGTABLE.2014.MeV

M       share/PDGTABLE.MeV
M       share/PDGTABLE.2014.MeV

============================== PartPropSvc v6r1 ==============================
! 2014-10-07 - Charles Leggett
 - added missing Geantinos to PDGTABLE.MeV and PDGTABLE.2014.MeV

M       share/PDGTABLE.2002.MeV
M       share/PDGTABLE.2014.MeV

! 2014-09-18 - Marco Clemencic
 - Updated CMake configuration.

============================== PartPropSvc v6r0 ==============================
! 2014-09-16 - Charles Leggett
 - updated PDGTABLE.MeV to 2011 numbers
 - moved old PDGTABLE.MeV to PDGTABLE.2002.MeV
 - added PDGTABLE.2014.MeV
 - deleted PDGTABLE (2002 in GeV) as nobody is using it

D       share/PDGTABLE
M       share/PDGTABLE.MeV
A       share/PDGTABLE.2002.MeV
A       share/PDGTABLE.2014.MeV

============================== PartPropSvc v5r1 ==============================
! 2014-03-18 - Ben Couturier
 - Fixed bug #104127: remove hwaf configuration.

============================== PartPropSvc v5r0 ==============================
! 2013-12-11 - Sebastien Binet
 - Added hwaf configuration files.

! 2013-07-19 - Marco Clemencic
 - Clean up in the use of the new PluginService.

! 2013-07-18 - Marco Clemencic
 - Removed use of obsolete AlgFactory.h, SvcFactory.h, ToolFactory.h,
   AudFactory.h and CnvFactory.h.

! 2013-07-17 - Marco Clemencic
 - Removed factories forward declarations.

! 2013-07-03 - Marco Clemencic
 - Removed explicit link to Reflex from CMakeLists.txt.

============================== PartPropSvc v4r6 ==============================
! 2012-11-14 - Marco Clemencic
 - Updated CMakeLists.txt.

============================== PartPropSvc v4r5 ==============================
! 2012-07-27 - Marco Clemencic
 - Rationalized the permissions of files in the repository.

============================== PartPropSvc v4r4 ==============================
! 2012-06-08 - Marco Clemencic
 - Added CMake configuration files.

============================== PartPropSvc v4r3 ==============================
! 2011-10-28 - Marco Clemencic
 - Changed the usage of make_pair to support C++0x.

============================== PartPropSvc v4r2 ==============================
! 2011-01-11 - Marco Clemencic
 - Fixed some ICC remarks.

============================== PartPropSvc v4r1 ==============================
! 2010-12-03 - Charles Leggett
 - Patch #76003: fix bug in initialize
   - change default location of PDGTABLE to remove "share" dir. Should
     really do this with a declare_runtime_extras to copy it to the Gaudi
     InstallArea/share dir, but this seems to be broken.

============================== PartPropSvc v4r0 ==============================
! 2010-08-18 - Charles Leggett
 - Patch #4323: allow user defined heavy ion handling in PartPropSvc
   - change way jobOpts handled: now assign type with each file
   - make log member var
   - allow user to specify UnknownParticleHandler function
   - instantiate PDT on first use, not before
   - PartPropSvc.cpp: handle heavy ions

============================= PartPropSvc v3r3 ===============================
! 2010-04-20 - Marco Clemencic
 - Removed ICC warnings and remarks.

================ PartPropSvc v3r2 ====================================
! 2009-07-21 - Hubert Degaudenzi
 - Fixed typo in the -no_static usage.

! 2009-07-20 - Marco Clemencic
 - Added '-no_static' to the declaration of the libraries (implemented since CMT
   v1r20p20081118).

================ PartPropSvc v3r1 ====================================
! 2009-06-10 - Marco Clemencic
 - Removed _dll.cpp and _load.cpp files (not needed anymore).

================ PartPropSvc v3r0 ====================================
! 2009-04-07 - Marco Clemencic
 - Moved back the namespace MSG to the global namespace.

! 2009-04-06 - Marco Clemencic
 - Renamed back the namespace Msg to MSG and moved it into the namespace Gaudi.

! 2009-02-19 - Marco Clemencic
 - Patch #2790: Rename the namespace MSG to Msg

! 2009-01-30 - Marco Clemencic
 - Replaced all the occurrences of endreq with endmsg.

! 2009-01-22 - Marco Clemencic
 - Adapted to the new IInterface functionalities (see GaudiKernel release.notes)

================ PartPropSvc v2r7 ====================================
! 20081028 Marco Clemencic
 - Removed extra "-lHepPID" from link options (already included in the LCG
   interface package).

================ PartPropSvc v2r6 =========================================
! 20070605 Paolo Calafiura
 Patch #1186.
 - added missing charged geantino.

================ PartPropSvc v2r5 =========================================
! 20070216 Charles Leggett
 Patch #1076
 - fix for default PDGTABLE.

================ PartPropSvc v2r4 =========================================
! 20070216 Charles Leggett
 Patch #1019
 - changed into a component lib
 - moved IPartPropSvc to GaudiKernel
 - moved PartPropSvc.h to src/

! 20070202 Hubert Degaudenzi
 - added missing library (CLHEP-HepPDT) in the CLHEP_linkopts macro
   (in the private section for the moment).
 - commented out the "apply_pattern install_runtime".

================ PartPropSvc v2r3p1 =========================================
! 20040910 Charles Leggett
 - src/PartPropSvc.cpp: uses PathResolver to locate PDT files
                        return FAILURE if can't find any files to open.

================ PartPropSvc v2r3 ==========================================
! 20040716 Charles Leggett
 - changed IID to InterfaceID

================ PartPropSvc v2r2 ==========================================
! 20040507 Charles Leggett
 - use standard macros in PartPropSvc_entries.cpp, PartPropSvc_load.cpp
 - removed PartPropSvc_dll.cpp

================ PartPropSvc v2r1 ==========================================
! 20040129 Paolo Calafiura
 - return an error if no PDT file was read
! 20040126 Charles Leggett
 - changed default data file to PDGTable.MeV. Obviously non-backward compatible

================ PartPropSvc v2r0 ==========================================
! 20040115 Paolo Calafiura
 - importing package to cvs repository"

! 20030624 Charles Leggett
 - removed built in version of HepPDT, uses CLHEP version instead
 - PartPropSvc.cpp: reads in Herwig and IsaJet as well
                  : removed QQ input mode
                  : added share directory, jobOptions and PDGTABLE file

! 20020703 Paolo Calafiura
 - TableBuilderT.icc: addQQParticles added return value (false) to
                      dummy implementation
